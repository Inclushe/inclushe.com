# [inclushe.com](https://inclushe.com)

## Install

Clone and `npm install`

## Usage

To develop locally and watch for changes, use `npm run watch`

To build, use `npm run build`

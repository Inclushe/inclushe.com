const projects = require('./projects.json')
const social = require('./social.json')

module.exports = {
  locals: {
    projects,
    social
  }
}
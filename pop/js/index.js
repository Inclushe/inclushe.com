(function () {
  var $ = document.querySelector.bind(document)
  var $$ = document.querySelectorAll.bind(document)
  var colors = ['#E91E63', '#FFEB3B', '#3F51B5']
  
  function createPopElement () {
    var popElement = document.createElement('div')
    var wx = window.innerHeight
    var wy = window.innerWidth
    var top = Math.floor((Math.random() * (wx - 100)))
    var left = Math.floor((Math.random() * (wy - 100)))
    popElement.className = 'pop'
    popElement.style.top = top + 'px'
    popElement.style.left = left + 'px'
    popElement.style.transform = 'rotate(' + (Math.floor(Math.random() * 100) - 50) + 'deg)'
    popElement.style.fontSize = (Math.floor(Math.random() * 100) + 20) + 'px'
    popElement.style.color = colors[(Math.floor(Math.random() * colors.length))]
    var popElementText = document.createTextNode('POP')
    popElement.appendChild(popElementText)
    document.body.appendChild(popElement)
  }
  
  function space (e) {
    if (e.code == 'Space') {
      if (e.type == "keydown") {
        $('#spacebar').setAttribute('style', 'transform: scale(0.9)')
      } else if (e.type == "keyup") {
        $('#spacebar').removeAttribute('style')
        $('#pop').currentTime = 0
        $('#pop').play()
        createPopElement()
      }
    }
  }
  
  document.addEventListener('keydown', space)
  document.addEventListener('keyup', space)
})()